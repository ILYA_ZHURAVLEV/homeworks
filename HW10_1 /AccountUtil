package org.levelup.lesson10.HW10_1;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AccountUtil {

    private static BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));

    static File file = new File("input.txt");

    // 1) СОЗДАНИЕ НОВОГО АККАУНТА И ЗАПИСЬ В ФАЙЛ
    public void createAccount() throws IOException {
        System.out.println("Вы выбрали: \"Создание нового счёта\".");

        String bankName = readBankName();
        Type typeOfAccount = readTypeOfAccount();
        Integer numberOfAccount = readNumberOfAccount();
        Long balance = readBalance();
        Date openAccountDate = readAccountDate("Введите дату открытия счёта");

        if (findSameNumberAccount(numberOfAccount)) {
            System.out.println("        Счёт с таким номером уже существует.");
            System.out.println("        Ошибка создания и записи счёта.\n");
        } else {
            Account newAccount = new Account(bankName, typeOfAccount, numberOfAccount, balance, openAccountDate);
            accountFileWriter(newAccount, true);
            System.out.println("Счёт успешно создан и записан в файл.\n");
        }
    }
    
    //// ВВОД И ПРОВЕРКА КОРРЕКТНОСТИ ДАННЫХ ДЛЯ ПОЛЕЙ АККАУНТА С КОНСОЛИ
    private String readBankName() throws IOException {
        System.out.print("    Введите название банка: ");
        return consoleReader.readLine();
    }
    
    private Type readTypeOfAccount() {
        boolean successTypeEnter = false;
        Type typeOfAccount = null;
        String readLine;
        Integer i;
        while (!successTypeEnter) {
            i = 0;
            System.out.print("    Выберите тип счёта: 1)RUS  2)EURO 3)DOLLAR: ");
            try {
                readLine = consoleReader.readLine();
                i = Integer.parseInt(readLine);
            } catch (IOException | NumberFormatException exc) {
                exc.getMessage();
            }
            switch (i) {
                case 1:
                    typeOfAccount = Type.RUS;
                    successTypeEnter = true;
                    break;
                case 2:
                    typeOfAccount = Type.EURO;
                    successTypeEnter = true;
                    break;
                case 3:
                    typeOfAccount = Type.DOLLAR;
                    successTypeEnter = true;
                    break;
                default:
                    System.out.println("        Неподходящие данные. Попробуйте ещй раз.");
                    break;
            }
        }
        //System.out.println(typeOfAccount);
        return typeOfAccount;
    }
    
    private Integer readNumberOfAccount() {
        boolean successNumberEnter = false;
        Integer numberOfAccount = 0;
        String readLine;
        while (!successNumberEnter) {
            System.out.print("    Введите номер счёта: ");
            try {
                readLine = consoleReader.readLine();
                numberOfAccount = Integer.parseInt(readLine);
                successNumberEnter = true;
            } catch (IOException | NumberFormatException exc) {
                System.out.println("        Неподходящие данные. Попробуйте ещё раз.");
            }
        }
        //System.out.println(numberOfAccount);
        return numberOfAccount;
    }
    
    private Long readBalance() {
        boolean successNumberEnter = false;
        Long balance = (long) 0;
        String readLine;
        while (!successNumberEnter) {
            System.out.print("    Введите данные баланса: ");
            try {
                readLine = consoleReader.readLine();
                balance = Long.parseLong(readLine);
                successNumberEnter = true;
            } catch (IOException | NumberFormatException exc) {
                System.out.println("        Неподходящие данные. Попробуйте ещй раз.");
            }
        }
        //System.out.println(balance);
        return balance;
    }
    
    private Date readAccountDate(String message) {
        boolean successAccountDate = false;
        Date openAccountDate = new Date();
        String readLine = "";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        while (!successAccountDate) {
            System.out.print("    " + message + " в формате \"дд.мм.гггг\": ");
            try {
                readLine = consoleReader.readLine();
                openAccountDate = simpleDateFormat.parse(readLine);
                successAccountDate = true;
            } catch (IOException | ParseException exc) {
                System.out.println("        Неподходящие данные. Попробуйте ещй раз.");
            }
        }
        //System.out.printf("Вы ввели дату: %td %tB %tY \n", openAccountDate, openAccountDate, openAccountDate);
        return openAccountDate;
    }
    
    //// ПРОВЕРКА НАЛИЧИЯ АККАУНТА В ФАЙЛЕ ПО НОМЕРУ
    private boolean findSameNumberAccount(Integer numberOfAccount) {
        if (file.exists()) {
            try (BufferedReader accountReader = new BufferedReader(new FileReader(file))) {
                String buf;
                while ((buf = accountReader.readLine()) != null) {
                    String[] strArray = new String[6];
                    strArray = buf.split(" - ", 7);
                    if (numberOfAccount == Integer.parseInt(strArray[2])) {
                        return true;
                    }
                }
            } catch (IOException exc) {
                System.out.println("        Произошла ошибка при чтении файла: " + exc.getMessage());
            }
        }
        return false;
    }
    
    //// ЗАПИСЬ АККАУНТА В ФАЙЛ И СОЗДАНИЕ ФАЙЛА ПРИ НЕОБХОДИМОСТИ
    private void accountFileWriter(Account account, boolean append) throws IOException {
        if (!file.exists()) { file.createNewFile(); }

        try (BufferedWriter accountWriter = new BufferedWriter(new FileWriter(file, append))) {
            accountWriter.write(account.accountInfo());
        } catch (IOException exc) {
            System.out.println("        Произошла ошибка записи в файл: " + exc.getMessage());
        }
    }

    // 2) ИЗМЕНЕНИЕ БАЛАНСА АККАУНТА ПО ЕГО НОМЕРУ
    public void changeBalance () throws IOException {
        if (!file.exists()) { file.createNewFile(); };
        System.out.println("Вы выбрали: \"Изменение баланса\".");
        Integer numberOfAccount = readNumberOfAccount();
        ArrayList<Account> accounts =  accountFileReaderToList();

        doThingsWithAccount some1 = (account) -> {
            Long balance = readBalance();
            account.setBalance(balance);
            System.out.println("Аккаунт найден и изменён\n");
            return;
        };

        findAccountAndDoSomething(accounts, numberOfAccount, some1);

        for (Account account : accounts) {
            accountFileWriter(account, true);
        }
    }

    // 3) ЗАКРЫТИЕ СЧЁТА
    public void closeAccount () throws IOException {
        if (!file.exists()) { file.createNewFile(); };
        System.out.println("Вы выбрали: \"Закрытие счёта\".");
        Integer numberOfAccount = readNumberOfAccount();
        ArrayList<Account> accounts =  accountFileReaderToList();

        doThingsWithAccount some2 = (account) -> {
            Date closeAccountDate;
            closeAccountDate = readAccountDate("Введите дату закрытия счёта");
            if (closeAccountDate.before(account.getOpenAccountDate())) {
                System.out.println("        Введена неверная дата - дата закрытия раньше даты создания.\n");
                return;
            }
            account.setCloseAccountDate(closeAccountDate);
            account.setOpened(false);
            System.out.println("Аккаунт найден и закрыт\n");
            return;
        };

        findAccountAndDoSomething(accounts, numberOfAccount, some2);

        for (Account account : accounts) {
            accountFileWriter(account, true);
        }
    }

    //// ЧТЕНИЕ ИЗ ФАЙЛА АККАУНТОВ В КОЛЛЕКЦИЮ И УДАЛЕНИЕ ФАЙЛА
    private ArrayList<Account> accountFileReaderToList() {
        ArrayList<Account> accountList = new ArrayList<>();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        try (BufferedReader accountReader = new BufferedReader(new FileReader(file))) {
            String buf;
            while ((buf = accountReader.readLine()) != null) {
                String[] strArray = new String[6];
                strArray = buf.split(" - ", 7);
                Account currentAccount = new Account(
                        strArray[0],
                        Type.valueOf(strArray[1]),
                        Integer.parseInt(strArray[2]),
                        Long.parseLong(strArray[3]),
                        simpleDateFormat.parse(strArray[4])
                );
                if (!(strArray[5].equals("null"))) {
                    currentAccount.setCloseAccountDate(simpleDateFormat.parse(strArray[5]));
                    currentAccount.setOpened(false);
                }
                accountList.add(currentAccount);
            }
        } catch (IOException | ParseException exc) {
            System.out.println("        Произошла ошибка при чтении файла: " + exc.getMessage());
        }
        file.delete();
        return accountList;
    }
    
    //// ПОИСК НУЖНОГО АККАУНТА И МАНИПУЛЯЦИИ С НИМ
    private void findAccountAndDoSomething (ArrayList<Account> accounts, Integer numberOfAccount, doThingsWithAccount some) {
            for (Account account : accounts) {
                if ((numberOfAccount == account.getNumberOfAccount()) & (account.isOpened())) {
                    some.someThings(account);
                    return;
                }
            }
            System.out.println("        Аккаунт не найден или закрыт\n");
            return;
    }

    private interface doThingsWithAccount {
        public void someThings(Account account);
    }

    // 4) ВЫВОД НА КОНСОЛЬ АККАУНТОВ ИЗ ФАЙЛА
    public void accountFilePrinter() throws IOException {
        if (!file.exists()) { file.createNewFile(); };
        try (BufferedReader accountReader = new BufferedReader(new FileReader(file))) {
            String buf;
            while ((buf = accountReader.readLine()) != null) {
                System.out.println(buf);
            }
        } catch (IOException exc) {
            System.out.println("        Произошла ошибка при чтении файла: " + exc.getMessage());
        }
        System.out.println();
    }

}